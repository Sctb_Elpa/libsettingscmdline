/**
 * @file settingscmdline.h
 * @brief Определение пространства имен SettingsCmdLine и класса cSettingsCmdLine
 */

#ifndef SETTINGSCMDLINE_H
#define SETTINGSCMDLINE_H

#include <QSettings>
#include <QVariantMap>

#ifdef WIN32
#include <windows.h>
#include <io.h>
#include <tchar.h>
#else
#define TCHAR		char
#define _T(x)		x
#define _tprintf	printf
#define _tmain		main
#endif

#include "SimpleOpt.h"

/*!
 * @mainpage
 *
 * @section SettingsCmdLine SettingsCmdLine
 * Библиотека - обработчик настроек программы на Qt.
 * Настраивается при помощи наследования. Поддерживает хранение настроек в .ini файле
 * @section Использование
 * * Необходимо объявить следующие обьекты:
 * enum enArgs - и заполнить его числовыми кодами аргументов командной строки
 * <br><b>Например:</b>
 * @code
 *  enum enArgs
 *  {
 *     OPT_HELP = 0,
 *     OPT_PORT,
 *     OPT_FROM
 *  };
 * @endcode
 * заполнить структуру CSimpleOpt::SOption g_rgOptions[]
 * <br><b>Например:</b>
 * @code
 *  CSimpleOpt::SOption g_rgOptions[] =
 *  {
 *      {
 *      OPT_HELP, _T("-h"), SO_NONE
 *      }, // "-h"
 *         //----------------
 *		{
 *		OPT_HELP, _T("--help"), SO_NONE
 *		}, // "--help"
 *		   //-----------------
 *		{
 *		OPT_PORT, _T("-p"), SO_REQ_SEP
 *		}, // "-p <PORT>"
 *		   //-----------------
 *      {
 *		OPT_FROM, _T("--from"), SO_REQ_CMB
 *		}, // "--from=<начальный пакет>"
 *		   //-----------------
 *  };
 * @endcode
 *  <br>Определить функцию
 * @code
 * void SetDefaultValues()
 * @endcode
 *  Которая установит критические значения по умолчанию
 *
 *  <br>Определить функцию
 * @code
 * void console_ShowUsage()
 * @endcode
 *  Она будет вызвана, если будет найден аргумент, у которго enArgs = 0
 *
 *  <br>Определить функцию
 * @code
 * struct key_val_res parse_Arg(int argCode, const char* optText, char* ArgVal)
 * @endcode
 *  которая будет парсить сами аргументы в удобоваримые представления, вернет true в случае удачи
 */

/**
 * @brief Пространство имен для операций с настройками программы
 */
namespace SettingsCmdLine
{
    /**
     * @class cSettingsCmdLine
     * @brief Класс, хранящий настройки программы в QVariantMap
     */
    class cSettingsCmdLine : public QVariantMap
    {
    public:
        /**
         * @param argc Количество аргументов командной строки
         * @param argv Массив указателей на строки аргументы командной строки
         * @param SettingsFilename Имя файла, ассоциированного с настройками (формат ini). Если не используется, то пустая строка
         * @param Table Заполненная таблица обратных вызовов
         */
        cSettingsCmdLine(int argc, char* argv[],
                         const QString& SettingsFilename,
                         struct pfTable *Table);

        ~cSettingsCmdLine();

        /**
         * @brief Прочитать значение, если оно существует
         * @param key Ключ
         * @param defaultVal Значение, возвращаемое функцией в случае отсутствия ключа key
         * @return value Значение, ассоциирование с ключем key таблицы настроек, или defaultVal, если ключа не найдено
         */
        QVariant valueIfExists(QString key, QVariant defaultVal) const;

        /**
         * @brief Проверка успешности анализа аргументов
         * @return true в случае успешности анализа ВСЕХ аргументов
         * @return false в случае ошибки
         */
        bool isOk() const;

    private:
        QSettings* SettingsInifile;
        bool fIsOk;
        struct pfTable *table;
    };

    typedef void (*pSetDefaultValues)(cSettingsCmdLine*);
    typedef void (*pfconsole_ShowUsage)(cSettingsCmdLine*);

    /**
     * @brief Таблица, возвращаемая функцией parse_Arg()
     */
    struct key_val_res
    {   /**
         * @brief Ключ
         */
        QString     key;
        /**
         * @brief Значение
         */
        QVariant    val;

        /**
         * @brief Флаг успешности
         */
        bool        res;
    };


    typedef struct key_val_res (*pparse_Arg)(int argCode,
                                             const char* optText,
                                             char* ArgVal,
                                             cSettingsCmdLine* origins);

    /**
     * @brief Таблица обратных вызовов. Используется для связывания с наследником
     */
    struct pfTable
    {
        /**
         * @brief Таблица соответствия кодов аргументов и их символьных представления
         */
        CSimpleOpt::SOption*    options_Table;
        /**
         * @brief Указатель на функцию, устанавливающущ значения по умолчанию
         */
        pSetDefaultValues       defaultValuesFun;
        /**
         * @brief Указатель на функцию, отображающую справку
         */
        pfconsole_ShowUsage     usageHelpDisplayFun;
        /**
         * @brief Указатель на функцию-парсер
         */
        pparse_Arg              parser;
    };

    /**
     * @brief Глобальный указатель на последний созанный объект cSettingsCmdLine
     *  Если в программе используется только один объкт класса cSettingsCmdLine можно
     *  обращаться к нему например так
     * @code
     * #include <settingscmdline/settingscmdline.h>
     *
     * ...
     *
     * (*settings)["someval"] = somevalue;
     * @endcode
     */
    extern cSettingsCmdLine* settings;
};

#endif // SETTINGSCMDLINE_H
